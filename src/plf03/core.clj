(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (* 2 x))
        g (fn [x] (* x x))
        h (fn [x] (+ (* 10 x) (* 5 x)))
        z (comp f g h)]
      (z 5)))


(defn función-comp-2
  []
  (let [f (fn [xs] (map inc xs))
        g (fn [xs] (filter even? xs))
        z (comp f g)]
    (z [1 2 3 4 5 6 7 8 9])))


(defn función-comp-3
  []
  (let [f (fn [xs] (map (fn [x] (* x x)) xs))
        g (fn [xs] (filter pos? xs))
        z (comp f g)]
    (z (range -10 15))))


(defn función-comp-4
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (take 5 xs))
        z (comp f g)]
    (z (map inc [8 9 10 11 14 17 15 18 25]))))


(defn función-comp-5
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (map (fn [x] (* x x)) xs))
        h (fn [xs] (take-last 6 xs))
        z (comp f g h)]
    (z (map inc [8 9 10 11 14 17 15 18 25]))))


(defn función-comp-6
  []
  (let [f (fn [xs] (sort > xs))
        g (fn [xs] (map (fn [x] ((partial * 5) x)) xs))
        h (fn [xs] (take-last 6 xs))
        z (comp f g h)]
    (z (map dec [-1 -3 7 9 11 12 17 10 9 14 13 18]))))


(defn función-comp-7
  []
  (let [f (fn [xs] (map (fn [x] ((partial + 2) x)) xs))
        g (fn [xs] (map (fn [x] ((partial * x) x)) xs))
        h (fn [xs] (sort xs))
        z (comp h f g)]
    (z [-1 -3 7 9 11 12 17 10 9 14 13 18])))


(defn función-comp-8
  []
  (let [f (fn [xs] (map (fn [x] ((partial + 10) x)) xs))
        g (fn [xs] (map (fn [x] ((partial * 6) x)) xs))
        h (fn [xs] (filter (comp not zero?) xs))
        z (comp f g h)]
    (z [0 1 0 2 0 3 0 4])))


(defn función-comp-9
  []
  (let [f (fn [xs] (reduce + xs))
        g (fn [xs] (sort >  (take 3 xs)))
        h (fn [xs] (filter (comp not zero?) xs))
        z (comp f g h)]
    (z [0 1 0 2 0 3 0 4 7 9 10 11 17 15])))


(defn función-comp-10
  []
  (let [f (fn [x] (even? x))
        g (fn [xs] (reduce + xs))
        h (fn [xs] (sort >  (take 3 xs)))
        i (fn [xs] (filter (comp not zero?) xs))
        z (comp f g h i)]
    (z [-1 0 1 0 2 0 3 0 4 7 9 10 11 17 15])))


(defn función-comp-11
  []
  (let [f (fn [xs] (apply min xs))
        g (fn [xs] (map (fn [x] ((partial * 0.5) x)) xs))
        h (fn [xs] (filter (comp not neg?) xs))
        z (comp f g h)]
    (z (map (fn [x] (- (* 5 x) (* 1/3 x))) [1 4 2 8 7 9 1 4 10]))))


(defn función-comp-12
  []
  (let [f (fn [xs] (into #{} xs))
        g (fn [xs] (map (fn [x] ((partial * 0.5) x)) xs))
        h (fn [xs] (drop-while pos? xs))
        z (comp f g h)]
    (z [2 1 5 -3 0 -2 -1])))


(defn función-comp-13
  []
  (let [f (fn [xs] (into [] xs))
        g (fn [xs] (map (fn [x] ((partial + 3) x)) xs))
        h (fn [xs] (drop-while neg? xs))
        z (comp f g h)]
    (z [2 1 5 -3 0 -2 -1])))

(defn función-comp-14
  []
  (let [f (fn [xs] (into '() xs))
        g (fn [xs] (map (fn [x] (+ (* 3 x) (* x x))) xs))
        h (fn [xs] (take-nth 3 xs))
        z (comp f g h)]
    (z [0 1 2 3 4 5 6 7 8 9])))


(defn función-comp-15
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (map inc xs))
        h (fn [xs] (filter odd? xs))
        z (comp f g h)]
    (z (map + [2 4 8] [1 3 5] [10 10 10]))))


(defn función-comp-16
  []
  (let [f (fn [xs] (into #{} xs))
        g (fn [xs] (filter (fn [y](> y 2)) (map (fn [x] (* x x)) xs)))
        h (fn [xs] (filter odd? xs))
        z (comp f g h)]
    (z (map + [2 4 8] [1 3 5] [10 10 10]))))


(defn función-comp-17
  []
  (let [f (fn [x] (* 2 x))
        g (fn [x] (* x x))
        h (fn [x] (int x))
        z (comp f g h)]
    (z 20)))


(defn función-comp-18
  []
  (let [f (fn [xs] (take-last 3 xs))
        g (fn [xs] (map char xs))
        z (comp f g)]
    (z (range 65 91))))


(defn función-comp-19
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (map char xs))
        z (comp f g)]
    (z (range 65 78))))


(defn función-comp-20
  []
  (let [f (fn [x] (* 2 x))
        g (fn [x] (* x x))
        h (fn [x] (int x))
        z (comp f g h h g f f)]
    (z 9)))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)


;complement
(defn función-complement-1
  []
  (let [f (fn [xs] (associative? xs))
        z (complement f)]
    (z {:a 10 :b 20 :c 40})))


(defn función-complement-2
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z 14)))


(defn función-complement-3
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z (first "Alonso"))))


(defn función-complement-4
  []
  (let [f (fn [xs] (empty? xs))
        z (complement f)]
    (z (filter odd? [2 4 6 8 10 12 14 16 18]))))


(defn función-complement-5
  [] 
  ;s: string 
  ;c: caracter
  (let [f (fn [s c] (some #(= c %) s))
        z (complement f)]
    (z "abc" \h)))


(defn función-complement-6
  []
  (let [f (fn [x] (> x 0))
        z (complement f)]
    (z 20)))
 

(defn función-complement-7
  []
  (let [f (fn [xs] (apply < xs))
        z (complement f)]
    (z [2 4 6 8])))


(defn función-complement-8
  []
  (let [f (fn [xs] (apply > xs))
        z (complement f)]
    (z [2 4 6 8])))


(defn función-complement-9
  []
  (let [f (fn [x] (/ x (* 2 x)))
        g (fn [x] (double? (f x)))
        z (complement g)]
    (z 7)))


(defn función-complement-10
  []
  (let [f (fn [x] (* x (* 2 x)))
        g (fn [x] (int? (f x)))
        z (complement g)]
    (z 1025)))


(defn función-complement-11
  []
  (let [f (fn [x] (list? x))
        z (complement f)]
    (z '(1 2 3 4))))


(defn función-complement-12
  []
  (let [f (fn [xs] (sequential? xs))
        z (complement f)]
    (z (range 1 20))))

(defn función-complement-13
  []
  (let [f (fn [x] (> x 0))
        z (complement f)]
    (z 90)))


(defn función-complement-14
  []
  (let [f (fn [x] (< x 0))
        z (complement f)]
    (z 87)))


(defn función-complement-15
  []
  (let [f (fn [x] (<= x 0))
        z (complement f)]
    (z 97)))


(defn función-complement-16
  []
  (let [f (fn [x] (zero? (/ x 3)))
        z (complement f)]
    (z 7)))


(defn función-complement-17
  []
  (let [f (fn [x] (and (> x 5) (< x 0)))
        z (complement f)]
    (z 1)))


(defn función-complement-18
  []
  (let [f (fn [x] (and (> x 0.8) (< x 0)))
        z (complement f)]
    (z 0.01)))

(defn función-complement-19
  []
  (let [f (fn [x] (or(> x 22) (< x -9)))
        z (complement f)]
    (z 100)))


(defn función-complement-20
  []
  (let [f (fn [a b c] (or (> a b) (< b c)))
        z (complement f)]
    (z 9 14 2)))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7) 
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)


;constantly
(defn función-constantly-1
  []
  (let [f (fn [x] (* x 3))
        z (constantly f)]
    ( (z) 100))) 


(defn función-constantly-2
  []
  (let [f (fn [x] (* x 3))
        g (fn [x] (inc (f x)))
        z (constantly g)]
    ((z) 185)))


(defn función-constantly-3
  []
  (let [f (fn [x] (+ x 3))
        g (fn [x] (inc (f x)))
        h (fn [x] (* x (g x)))
        z (constantly h)]
    ((z) 170)))


(defn función-constantly-4
  []
  (let [f (fn [xs] (filter even? xs))
        z (constantly f)]
    ((z) (range 5 20))))


(defn función-constantly-5
  []
  (let [f (fn [xs] (into [] xs))
        g (fn [xs] ( f (take 10 xs)))
        h (fn [x] (g (repeat 30 x)))
        z (constantly h)]
    ((z) 10)))


(defn función-constantly-6
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (f (take 5 xs)))
        z (constantly g)]
    ((z) #{1 2 3 4 5 9 8 7 6 10})))


(defn función-constantly-7
  []
  (let [f (fn [xs] (into [] xs))
        g (fn [xs] (f (take-nth 2 xs)))
        z (constantly g)]
    ((z) [7 8 9 10 11 12 14 17 18 19 20])))


(defn función-constantly-8
  []
  (let [f (fn [x] (int x))
        g (fn [a b] (f (if (> a 10) (+ 2 b) (* 2 b))))
        z (constantly g)]
    ((z) 2 3)))


(defn función-constantly-9
  []
  (let [f (fn [x] (* x 10))
        g (fn [a b] (f (if (even? a) (+ 3 b) (* 7 b))))
        z (constantly g)]
    ((z) 3 2)))


(defn función-constantly-10
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [xs] (f (map inc xs)))
        z (constantly g)]
    ((z) (map + [2 4 8] [1 3 5]))))


(defn función-constantly-11
  []
  (let [f (fn [xs] (split-with (partial > 10) xs))
        g (fn [xs] (f (map inc xs)))
        z (constantly g)]
    ((z) [1 2 3 2 1 14 17 16 18])))


(defn función-constantly-12
  []
  (let [f (fn [xs] (split-with (partial > 10) xs))
        g (fn [xs] (f (filter even? xs)))
        z (constantly g)]
    ((z) [1 2 3 2 1 14 17 16 18])))


(defn función-constantly-13
  []
  (let [f (fn [xs] (reverse  xs))
        g (fn [xs] (f (map (fn [x] (- (* 5 x) (* 1/3 x))) xs)))
        z (constantly g)]
    ((z) [1 4 2 8 7 9 1 4 10])))


(defn función-constantly-14
  []
  (let [f (fn [xs] (sort > xs))
        g (fn [xs] (f (map (fn [x] (- (* 5 x) (* 3 x))) xs)))
        z (constantly g)]
    ((z) [1 4 -1 -2 -7 -8 0 9 10 12 14 9 24 17 6 0])))


(defn función-constantly-15
  []
  (let [f (fn [xs] (into #{} xs))
        g (fn [xs] (f (map (fn [x] (- (* 5 x) (* 3 x))) xs)))
        z (constantly g)]
    ((z) [1 4 -1 -2 -7 -8 0 9 10 12 14 9 24 17 6 0 10 7 8 91 14 17])))


(defn función-constantly-16
  []
  (let [f (fn [xs] (merge-with + xs xs xs xs xs))
        z (constantly f)]
    ((z) {:a 10 :b 15 :c 20 :e 245})))


(defn función-constantly-17
  []
  (let [f (fn [xs] (sort xs))
        g (fn [xs] (f (filter  xs)))
        z (constantly g)]
    ((z) [1 2 3 2 1 14 17 16 18])))


(defn función-constantly-18
  []
  (let [f (fn [xs] (into '() xs))
        g (fn [xs] (f (split-with odd? xs)))
        z (constantly g)]
    ((z) (range 1 20))))


(defn función-constantly-19
  []
  (let [f (fn [xs] (apply + xs))
        g (fn [xs] (f (map + [2 4 8] [1 3 5] [10 10 10] xs)))
        z (constantly g)]
    ((z) [6 6 6])))


(defn función-constantly-20
  []
  (let [f (fn [xs] (every? (fn [x] (> x 0)) xs))
        z (constantly f)]
    ((z) [1 2 3 4 5])))


(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)


;every-pred
(defn función-every-pred-1
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z 3 7 9)))


(defn función-every-pred-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (ratio? x))
        z (every-pred f g)
        ]
    (z -2/3)))


(defn función-every-pred-3
  []
  (let [f (fn [s] (comp (partial > 10) count) s)
        g (fn [s] (string? s))
        z (every-pred f g)]
    (z "Alonso")))


(defn función-every-pred-4
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (even? x))
        z (every-pred f g)]
    (z 2 4 6)))


(defn función-every-pred-5
  []
  (let [f (fn [xs] (even? (count xs)))
        g (fn [xs] (vector? xs))
        z (every-pred f g)]
    (z [2 4 6 8 9 10 14])))


(defn función-every-pred-6
  []
  (let [f (fn [xs] (contains? xs 5))
        g (fn [xs] (set? xs))
        z (every-pred f g)]
    (z #{1 7 8 9 10 12 14})))


(defn función-every-pred-7
  []
  (let [f (fn [xs] (contains? xs nil))
        g (fn [xs] (set? xs))
        z (every-pred f g)]
    (z #{nil 0 1 9 14 17 19 20})))

(defn función-every-pred-8
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (decimal? x))
        z (every-pred f g)]
    (z -1.5M)))


(defn función-every-pred-9
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (pos? x))
        h (fn [x] (int? x))
        z (every-pred f g h)]
    (z 14)))


(defn función-every-pred-10
  []
  (let [f (fn [x] (odd? x))
        g (fn [x] (pos? x))
        h (fn [x] (int? x))
        z (every-pred f g h)]
    (z 17)))


(defn función-every-pred-11
  []
  (let [f (fn [xs] (indexed? xs))
        g (fn [xs] (vector? xs))
        z (every-pred f g)]
    (z [1 2 3 4 5 6 7 8 9])))


(defn función-every-pred-12
  []
  (let [f (fn [xs] (keyword? (key (first xs))))
        g (fn [xs] (map? xs))
        z (every-pred f g)]
    (z {:a 1 :b 2 :c 3 :d 4 :e 5})))


(defn función-every-pred-13
  []
  (let [f (fn [xs] (keyword? (key (first xs))))
        g (fn [xs] (map? xs))
        z (every-pred f g)]
    (z {1 2 3 4 5 6 7 8 9 10})))


(defn función-every-pred-14
  []
  (let [f (fn [xs] (pos? (first xs)))
        g (fn [xs] (rational? (first xs)))
        h (fn [xs] (vector? xs))
        z (every-pred f g h)]
    (z [-1/2 2/4 3/5 9/7])))


(defn función-every-pred-15
  []
  (let [f (fn [xs] (neg? (last xs)))
        g (fn [xs] (rational? (last xs)))
        h (fn [xs] (vector? xs))
        z (every-pred f g h)]
    (z [-1/2 2/4 3/5 -9/7])))


(defn función-every-pred-16
  []
  (let [f (fn [xs] (apply > xs))
        g (fn [xs] (seq? xs))
        z (every-pred f g)]
    (z (range 1 9))))


(defn función-every-pred-17
  []
  (let [f (fn [xs] (apply < xs))
        g (fn [xs] (seq? xs))
        z (every-pred f g)]
    (z (range 14 20))))


(defn función-every-pred-18
  []
  (let [f (fn [x] (> x 80))
        g (fn [x] (number? x))
        z (every-pred f g)]
    (z -5)))

(defn función-every-pred-19
  []
  (let [f (fn [x] (and (< x 100) (> x 6)))
        g (fn [x] (number? x))
        z (every-pred f g)]
    (z 17)))


(defn función-every-pred-20
  []
  (let [f (fn [x] (or (< x (* x x)) (> x (* 6 x))))
        g (fn [x] (number? x))
        z (every-pred f g)]
    (z 17)))


(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)


;fnil
(defn función-fnil-1
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (f (+ x 2)))
        h (fn [x y] (g (+ x y)))
        z (fnil h 7)]
    (z nil 10)))


(defn función-fnil-2
  []
  (let [f (fn [x y] (if (< x y) (+ x y) (- x y)))
        z (fnil f 5)]
    (z nil 4)))


(defn función-fnil-3
  []
  (let [f (fn [xs] (map (fn [x] (* x x)) xs))
        g (fn [pred xs] (f (filter pred xs)))
        z (fnil g pos?)]
    (z nil (range -5 15))))


(defn función-fnil-4
  []
  (let [f (fn [num xs] (take num xs))
        z (fnil f 5)]
    (z nil (map inc [8 9 10 11 14 17 15 18 25]))))


(defn función-fnil-5
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (f (map (fn [x] (* x x)) xs)))
        h (fn [num xs] (g (take-last num xs)))
        z (fnil h 5 )]
    (z nil (map inc [8 9 10 11 14 17 15 18 25]))))


(defn función-fnil-6
  []
  (let [f (fn [xs] (sort > xs))
        g (fn [xs] (f (map (fn [x] ((partial * 5) x)) xs)))
        h (fn [num xs] (g (take-last num xs)))
        z (fnil h 5)]
    (z nil (map dec [-1 -3 7 9 11 12 17 10 9 14 13 18]))))


(defn función-fnil-7
  []
  (let [f (fn [xs] (map (fn [x] ((partial + 2) x)) xs))
        g (fn [y xs] (f (map (fn [x] ((partial * x) y)) xs)))
        z (fnil g 8)]
    (z nil [-1 -3 7 9 11 12 17 10 9 14 13 18])))


(defn función-fnil-8
  []
  (let [f (fn [xs] (map (fn [x] ((partial + 10) x)) xs))
        g (fn [pred xs] (f (filter (comp not pred) xs)))
        z (fnil g zero?)]
    (z nil [0 1 0 2 0 3 0 4])))


(defn función-fnil-9
  []
  (let [f (fn [xs] (reduce + xs))
        g (fn [num xs] (f(sort >  (take num xs))))
        z (fnil g 7)]
    (z nil [0 1 0 2 0 3 0 4 7 9 10 11 17 15])))


(defn función-fnil-10
  []
  (let [f (fn [x] (even? x))
        g (fn [pred xs] ( f(first (filter (comp not pred) xs))))
        z (fnil g zero?)]
    (z nil [-1 0 1 0 2 0 3 0 4 7 9 10 11 17 15])))


(defn función-fnil-11
  []
  (let [f (fn [xs] (map (fn [x] ((partial * 0.5) x)) xs))
        g (fn [pred xs] (f (filter (comp not pred) xs)))
        z (fnil g neg?)]
    (z nil (map (fn [x] (- (* 5 x) (* 1/3 x))) [1 4 2 8 7 9 1 4 10]))))


(defn función-fnil-12
  []
  (let [f (fn [xs] (into #{} xs))
        h (fn [pred xs] (f (drop-while pred xs)))
        z (fnil h pos?)]
    (z nil [2 1 5 -3 0 -2 -1])))


(defn función-fnil-13
  []
  (let [f (fn [xs] (into [] xs))
        g (fn [xs] (f (map (fn [x] ((partial + 3) x)) xs)))
        h (fn [pred xs] (g (drop-while pred xs)))
        z (fnil h neg?)]
    (z nil [2 1 5 -3 0 -2 -1])))

(defn función-fnil-14
  []
  (let [f (fn [xs] (into '() xs))
        g (fn [xs] (f (map (fn [x] (+ (* 3 x) (* x x))) xs)))
        h (fn [num xs] (g (take-nth num xs)))
        z (fnil h 3)]
    (z nil [0 1 2 3 4 5 6 7 8 9])))


(defn función-fnil-15
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (f (map inc xs)))
        h (fn [pred xs] (g (filter pred xs) ))
        z (fnil h odd?)]
    (z nil (map + [2 4 8] [1 3 5] [10 10 10]))))


(defn función-fnil-16
  []
  (let [f (fn [xs] (into #{} xs))
        g (fn [pred xs] (f (filter pred xs)))
        z (fnil g odd?)]
    (z nil (map + [2 4 8] [1 3 5] [10 10 10]))))


(defn función-fnil-17
  []
  (let [f (fn [x] (* 2 x))
        g (fn [x y] (f(/ x (* 0.5 y))))
        z (fnil g 10)]
    (z nil 20)))
 

(defn función-fnil-18
  []
  (let [f (fn [num xs] (take-last num xs))
        g (fn [num xs] (f num (map char xs)))
        z (fnil g 5)]
    (z nil (range 65 91))))


(defn función-fnil-19
  []
  (let [g (fn [xs] (reverse xs))
        h (fn [f xs] (g (map f xs)))
        z (fnil h char)]
    (z nil (range 65 78))))


(defn función-fnil-20
  []
  (let [f (fn [x] (* x x))
        g (fn [x] (f (* (* x x) (- (* 5 x) (- x 5)))))
        h (fn [x y] (g (dec (+ x y))))
        z (fnil h 8)]
    (z nil 7)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)



;juxt
(defn función-juxt-1
  []
  (let [f (fn [x] (* x x))
        g (fn [x] (f (* (* x x) (- (* 5 x) (- x 5)))))
        h (fn [x] (dec x))
        z (juxt f g h)]
    (z 7)))


(defn función-juxt-2
  []
  (let [f (fn [xs] (map inc xs))
        g (fn [xs] (filter even? xs))
        z (juxt f g)]
    (z [1 2 3 4 5 6 7 8 9])))


(defn función-juxt-3
  []
  (let [f (fn [xs] (map (fn [x] (* x x)) xs))
        g (fn [xs] (filter pos? xs))
        z (juxt f g)]
    (z (range -10 15))))


(defn función-juxt-4
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (take 5 xs))
        z (juxt f g)]
    (z (map inc [8 9 10 11 14 17 15 18 25]))))


(defn función-juxt-5
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (map (fn [x] (* x x)) xs))
        h (fn [xs] (take-last 6 xs))
        z (juxt f g h)]
    (z (map inc [8 9 10 11 14 17 15 18 25]))))


(defn función-juxt-6
  []
  (let [f (fn [xs] (sort > xs))
        g (fn [xs] (map (fn [x] ((partial * 5) x)) xs))
        h (fn [xs] (take-last 6 xs))
        z (juxt f g h)]
    (z (map dec [-1 -3 7 9 11 12 17 10 9 14 13 18]))))


(defn función-juxt-7
  []
  (let [f (fn [xs] (map (fn [x] ((partial + 2) x)) xs))
        g (fn [xs] (map (fn [x] ((partial * x) x)) xs))
        h (fn [xs] (sort xs))
        z (juxt h f g)]
    (z [-1 -3 7 9 11 12 17 10 9 14 13 18])))


(defn función-juxt-8
  []
  (let [f (fn [xs] (map (fn [x] ((partial + 10) x)) xs))
        g (fn [xs] (map (fn [x] ((partial * 6) x)) xs))
        h (fn [xs] (filter (comp not zero?) xs))
        z (juxt f g h)]
    (z [0 1 0 2 0 3 0 4])))


(defn función-juxt-9
  []
  (let [f (fn [xs] (reduce + xs))
        g (fn [xs] (sort >  (take 3 xs)))
        h (fn [xs] (filter (comp not zero?) xs))
        z (juxt f g h)]
    (z [0 1 0 2 0 3 0 4 7 9 10 11 17 15])))


(defn función-juxt-10
  []
  (let [f (fn [xs] (even? (first xs)))
        g (fn [xs] (reduce + xs))
        h (fn [xs] (sort >  (take 3 xs)))
        i (fn [xs] (filter (comp not zero?) xs))
        z (juxt f g h i)]
    (z [-1 0 1 0 2 0 3 0 4 7 9 10 11 17 15])))


(defn función-juxt-11
  []
  (let [f (fn [xs] (apply min xs))
        g (fn [xs] (map (fn [x] ((partial * 0.5) x)) xs))
        h (fn [xs] (filter (comp not neg?) xs))
        z (juxt f g h)]
    (z (map (fn [x] (- (* 5 x) (* 1/3 x))) [1 4 2 8 7 9 1 4 10]))))


(defn función-juxt-12
  []
  (let [f (fn [xs] (into #{} xs))
        g (fn [xs] (map (fn [x] ((partial * 0.5) x)) xs))
        h (fn [xs] (drop-while pos? xs))
        z (juxt f g h)]
    (z [2 1 5 -3 0 -2 -1])))


(defn función-juxt-13
  []
  (let [f (fn [xs] (into [] xs))
        g (fn [xs] (map (fn [x] ((partial + 3) x)) xs))
        h (fn [xs] (drop-while neg? xs))
        z (juxt f g h)]
    (z [2 1 5 -3 0 -2 -1])))

(defn función-juxt-14
  []
  (let [f (fn [xs] (into '() xs))
        g (fn [xs] (map (fn [x] (+ (* 3 x) (* x x))) xs))
        h (fn [xs] (take-nth 3 xs))
        z (juxt f g h)]
    (z [0 1 2 3 4 5 6 7 8 9])))


(defn función-juxt-15
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (map inc xs))
        h (fn [xs] (filter odd? xs))
        z (juxt f g h)]
    (z (map + [2 4 8] [1 3 5] [10 10 10]))))


(defn función-juxt-16
  []
  (let [f (fn [xs] (into #{} xs))
        g (fn [xs] (filter (fn [y] (> y 2)) (map (fn [x] (* x x)) xs)))
        h (fn [xs] (filter odd? xs))
        z (juxt f g h)]
    (z (map + [2 4 8] [1 3 5] [10 10 10]))))


(defn función-juxt-17
  []
  (let [f (fn [xs ] (filter odd? xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (filter pos? xs))
        z (juxt f g h)]
    (z (range -1 20))))


(defn función-juxt-18
  []
  (let [f (fn [xs] (take-last 3 xs))
        g (fn [xs] (map char xs))
        z (juxt f g)]
    (z (range 65 91))))


(defn función-juxt-19
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (map char xs))
        z (juxt f g)]
    (z (range 65 78))))


(defn función-juxt-20
  []
  (let [f (fn [x] (+ 125 x))
        g (fn [x] (* x x))
        h (fn [x] (odd? x))
        z (juxt f g h f g)]
    (z 9)))


(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)


;partial
(defn función-partial-1
   []
   (let [f (fn [a b c] (+ (* 10 a) (* b (* b c))))
         z (partial f 10 4)]
     (z 5)))


(defn función-partial-2
  []
  (let [f (fn [xs] (map inc xs))
        g (fn [pred xs] (f(filter pred xs)))
        z (partial g even?)]
    (z [1 2 3 4 5 6 7 8 9])))


(defn función-partial-3
  []
  (let [f (fn [xs] (map (fn [x] (* x x)) xs))
        g (fn [pred xs] (f (filter pred xs)))
        z (partial g pos?)]
    (z (range -10 15))))


(defn función-partial-4
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [num xs] (f (take num xs)))
        z (partial g 5)]
    (z (map dec [8 9 10 11 14 17 15 18 25]))))


(defn función-partial-5
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (f (map (fn [x] (* x x)) xs)))
        h (fn [num xs] (g (take-last num xs)))
        z (partial h 6)]
    (z (map inc [8 9 10 11 14 17 15 18 25]))))


(defn función-partial-6
  []
  (let [f (fn [xs] (sort > xs))
        g (fn [xs] (f (map (fn [x] ((partial * 5) x)) xs)))
        h (fn [xs num] (g (take-last num xs)))
        z (partial h [-1 -3 7 9 11 12 17 10 9 14 13 18])]
    (z 4)))


(defn función-partial-7
  []
  (let [f (fn [x xs] (map (fn [y] ((partial * y) x)) xs))
        g (fn [x xs] (f x (sort xs)))
        z (partial g 5)]
    (z [-1 -3 7 9 11 12 17 10 9 14 13 18])))


(defn función-partial-8
  []
  (let [f (fn [xs] (map (fn [x] ((partial + 10) x)) xs))
        g (fn [xs] (f (map (fn [x] ((partial * 6) x)) xs)))
        h (fn [pred xs] ( g (filter (comp not pred) xs)))
        z (partial h zero?)]
    (z [0 1 0 2 0 3 0 4])))


(defn función-partial-9
  []
  (let [f (fn [xs] (reduce + xs))
        g (fn [num xs] (f (sort >  (take num xs))))
        h (fn [pred num xs] (g num (filter (comp not pred) xs)))
        z (partial h zero?)]
    (z 5 [0 1 0 2 0 3 0 4 7 9 10 11 17 15])))


(defn función-partial-10
  []
  (let [f (fn [x] (even? x))
        g (fn [xs] (f (reduce + xs)))
        h (fn [xs] (g (sort >  (take 3 xs))))
        i (fn [pred xs] (h (filter (comp not pred) xs)))
        z (partial i zero?)]
    (z [-1 0 1 0 2 0 3 0 4 7 9 10 11 17 15])))


(defn función-partial-11
  []
  (let [f (fn [a b c] (* a b c))
        g (fn [a b c] (f (+ a b c) (- a b c) (* (* 2 a) (* b c))))
        h (fn [a b c] (g a b c))
        z (partial h 5)]
    (z 10 7)))

(defn función-partial-12
  []
  (let[f (fn [pred xs] (filterv pred xs))
        z (partial f (fn [x] (= (count x) 1)))]
    (z ["araña" "abeja" "b" "n" "f" "lisp" "función" "l" ""])))


(defn función-partial-13
  []
  (let [f (fn [f xs] (group-by f xs))
        z (partial f (fn [y] (> y 40)))]
    (z (map (fn [x] (* x x)) [2 4 6 8 10 11 12 13 14 15 16]))))


(defn función-partial-14
  []
  (let [f (fn [num f x] (take num (iterate f x)))
        z (partial f 10)]
    (z (partial * 2)  1)))


(defn función-partial-15
  []
  (let [f (fn [f xs] (keep f xs))
        z (partial f #(if (pos? %) %))]
    (z (range -5 10))))


(defn función-partial-16
  []
  (let [f (fn [f xs] (keep-indexed f xs))
        z (partial f (fn [id v]
                       (if (even? v) id)))]
    (z [-9 0 29 -7 45 3 -8])))

(defn función-partial-17
  []
  (let [f (fn [f xs] (map-indexed f xs))
        z (partial f)]
    (z (fn [idx itm] [idx itm]) "Alonso Garcia")))


(defn función-partial-18
  []
  (let [f (fn [f c1 c2 c3] (mapv f c1 c2 c3))
        z (partial f +)]
    (z [1 2 3] (iterate inc 1) [2 5 6])))


(defn función-partial-19
  []
  (let [f (fn [f map map2 map3] (merge-with f map map2 map3))
        z (partial f *)]
    (z {:a 10 :b 15 :c 20}
       {:a 1  :b 15 :c 20 :d 147}
       {:a 10 :b 15 :c 20 :e 245})))


(defn función-partial-20
  []
  (let [f (fn [xs] (into [] xs))
        g (fn [xs] (f (map (fn [x] ((partial + 3) x)) xs)))
        h (fn [pred xs] (g (drop-while pred xs)))
        z (partial h neg?)]
    (z [2 1 5 -3 0 -2 -1])))


(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)


;some-fn
(defn función-some-fn-1
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (some-fn f g)]
    (z 37)))


(defn función-some-fn-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (ratio? x))
        z (some-fn f g)]
    (z -7/8)))


(defn función-some-fn-3
  []
  (let [f (fn [s] (string? s))
        z (some-fn f)]
    (z 68)))


(defn función-some-fn-4
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (char? x))
        z (some-fn f g)]
    (z \n )))


(defn función-some-fn-5
  []
  (let [f (fn [xs] (even? (count xs)))
        g (fn [xs] (vector? xs))
        z (some-fn f g)]
    (z [2 -14 -6 8 9 -1 14])))


(defn función-some-fn-6
  []
  (let [f (fn [xs] (contains? xs 50))
        g (fn [xs] (set? xs))
        z (some-fn f g)]
    (z #{1 7 8 9 10 12 14 27 19 2 21 34})))


(defn función-some-fn-7
  []
  (let [f (fn [xs] (contains? xs nil))
        g (fn [xs] (set? xs))
        z (some-fn f g)]
    (z [nil 1 7 8 9 10 12 14])))

(defn función-some-fn-8
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (decimal? x))
        z (some-fn f g)]
    (z -1.89M)))


(defn función-some-fn-9
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (pos? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z -16)))


(defn función-some-fn-10
  []
  (let [f (fn [x] (odd? x))
        g (fn [x] (pos? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z 27)))


(defn función-some-fn-11
  []
  (let [f (fn [xs] (indexed? xs))
        g (fn [xs] (vector? xs))
        z (some-fn f g)]
    (z [1 2 3 4 5 6 7 8 9])))


(defn función-some-fn-12
  []
  (let [f (fn [xs] (keyword? (key (first xs))))
        g (fn [xs] (map? xs))
        z (some-fn f g)]
    (z {:a 1 :b 21 :c 3 :d 4 :e 15})))


(defn función-some-fn-13
  []
  (let [f (fn [xs] (keyword? (key (first xs))))
        g (fn [xs] (map? xs))
        z (some-fn f g)]
    (z {1 2 3 4 5 6})))


(defn función-some-fn-14
  []
  (let [f (fn [xs] (pos? (first xs)))
        g (fn [xs] (rational? (first xs)))
        h (fn [xs] (vector? xs))
        z (some-fn f g h)]
    (z [-1/2 2/4 3/5 9/7])))


(defn función-some-fn-15
  []
  (let [f (fn [xs] (neg? (last xs)))
        g (fn [xs] (rational? (last xs)))
        h (fn [xs] (vector? xs))
        z (some-fn f g h)]
    (z [-1/2 2/4 3/5 -9/7])))


(defn función-some-fn-16
  []
  (let [f (fn [xs] (apply > xs))
        g (fn [xs] (seq? xs))
        z (some-fn f g)]
    (z (range 1 9))))


(defn función-some-fn-17
  []
  (let [f (fn [xs] (apply < xs))
        g (fn [xs] (seq? xs))
        z (some-fn f g)]
    (z (range 14 20))))


(defn función-some-fn-18
  []
  (let [f (fn [x] (> x 80))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z -61)))

(defn función-some-fn-19
  []
  (let [f (fn [x] (and (< x 100) (> x 6)))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z 39)))


(defn función-some-fn-20
  []
  (let [f (fn [x] (or (< x (* x x)) (> x (* 6 x))))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z 45)))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)
